const validateIsNumberInputs = require('./validate_is_number_inputs');

function sum(a, b) {
  validateIsNumberInputs(a, b);
  return a + b;
}

module.exports = sum;
